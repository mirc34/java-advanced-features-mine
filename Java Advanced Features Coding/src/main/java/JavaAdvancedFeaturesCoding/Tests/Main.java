package JavaAdvancedFeaturesCoding.Tests;

public class Main {
    public static void main(String[] args) {
        Runnable x = new RandomAccessArrayListTest(10);
        Runnable y = new RandomAccessLinkedListTest(10);
        MyTimer t = new MyTimer(x);
        MyTimer t2 = new MyTimer(y);
        t.time();
        t2.time();

        InsertArrayListTest insArrayList = new InsertArrayListTest(10);
        InsertLinkedListTest insLinkedList = new InsertLinkedListTest(10);
        insArrayList.run();
        insLinkedList.run();
        insArrayList.printList();
        System.out.println();
        insLinkedList.printList();
//        MyTimer t4 = new MyTimer(insLinkedList);
//        MyTimer t3 = new MyTimer(insArrayList);
//        t3.time();
//        t4.time();
    }
}