package JavaAdvancedFeaturesCoding.Tests;

import java.util.LinkedList;

public class InsertLinkedListTest implements Runnable{
    LinkedList<Integer> linkedList = new LinkedList<>();
    int size;

    public InsertLinkedListTest(int size) {
        this.size = size;
    }

    public void run() {
        for(int i = 0; i < size; i++)
            linkedList.add(0, i);
    }

    public void printList() {
        for(int i = 0; i < size; i++)
            System.out.println(linkedList.get(i));
    }

    @Override
    public String toString() {
        return "InsertLinkedListTest";
    }
}
