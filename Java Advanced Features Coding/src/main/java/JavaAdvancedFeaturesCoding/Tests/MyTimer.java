package JavaAdvancedFeaturesCoding.Tests;

class MyTimer {
    private Runnable action;
    public MyTimer(Runnable action) {
        this.action = action;
    }
    public long time() {
        long before = System.nanoTime();
        action.run();
        long after = System.nanoTime();
        long diff = after - before;
        System.out.println("Testing " + action + " " + diff + " nanos");
        return diff;
    }
}

