package JavaAdvancedFeaturesCoding.Tests;

import java.util.ArrayList;

public class InsertArrayListTest implements Runnable{

    ArrayList<Integer> arrayList = new ArrayList<>();
    int size;

    public InsertArrayListTest(int size) {
        this.size = size;
    }

    public void run() {
        for(int i = 0; i < size; i++)
            arrayList.add(0, i);
    }

    public void printList() {
        for(int i = 0; i < size; i++)
            System.out.println(arrayList.get(i));
    }

    @Override
    public String toString() {
        return "InsertArrayListTest";
    }
}
