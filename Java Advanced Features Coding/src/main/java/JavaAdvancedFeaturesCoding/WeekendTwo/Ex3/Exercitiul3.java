package JavaAdvancedFeaturesCoding.WeekendTwo.Ex3;

class Print implements Runnable{
    public void run() {
        System.out.println("Hello");
    }

    @Override
    public String toString() {
        return "Print";
    }
}

class ForLoop implements Runnable{
    public void run() {
        for(int i = 0; i < 100000; i++);
    }
    @Override
    public String toString() {
        return "ForLoop";
    }
}

class Quadratic implements Runnable{
    public void run() {
        for(int i = 0; i < 100000; i++)
            for(int j = 0; j < 100000; j++);
    }
    @Override
    public String toString() {
        return "Quadratic";
    }
}

class Timer {
    private Runnable action;
    public Timer(Runnable action) {
        this.action = action;
    }
    public long time() {
        long before = System.nanoTime();
        action.run();
        long after = System.nanoTime();
        long diff = after - before;
        System.out.println("Testing " + action + " " + diff + " nanos");
        return diff;
    }
}

public class Exercitiul3 {
    public static void main(String[] args) {
           Print p = new Print();
           Timer t = new Timer(p);
           t.time();
           t = new Timer(new ForLoop());
           t.time();
           t = new Timer(new Quadratic());
           t.time();
    }
}
