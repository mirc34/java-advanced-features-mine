package JavaAdvancedFeaturesCoding.WeekendTwo.Ex4;

import java.util.Scanner;

public abstract class BaseMenu implements Menu{
    protected Menu previous;
    protected Scanner input = new Scanner(System.in);

    public BaseMenu(Menu previous) {
        this.previous = previous;
    }
}
