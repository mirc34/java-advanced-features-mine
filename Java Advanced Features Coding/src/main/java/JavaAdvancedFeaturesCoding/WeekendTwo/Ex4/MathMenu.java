package JavaAdvancedFeaturesCoding.WeekendTwo.Ex4;

public class MathMenu extends BaseMenu{

    public MathMenu(Menu previous) {
        super(previous);
    }

    public void draw() {
        System.out.println("1 -> Logica");
        System.out.println("2 -> Analiza functiilor");
        System.out.println("3 -> Ecuatii diferentiale");
        System.out.println("4 -> Meniu anterior");
    }

    public Menu select() {
        String userInput = input.nextLine();
        if(userInput.equals("1")) {
            return new LogicMenu(this);
        } else if(userInput.equals("2")) {
            System.out.println("Meniu neimplementat");
            return this;
        } else if(userInput.equals("3")) {
            System.out.println("Meniu neimplementat");
            return this;
        } else if(userInput.equals("4")) {
            return previous;
        } else {
            System.out.println("Optiune invalida");
            return this;
        }
        //return null;
    }
}
