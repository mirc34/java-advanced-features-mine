package JavaAdvancedFeaturesCoding.WeekendTwo.Ex4;

public class MainMenu extends BaseMenu{

    public MainMenu() {
        super(null);
    }

    public void draw() {
        System.out.println("1 --> Probleme de matematica");
        System.out.println("2 --> Probleme de informatica");
        System.out.println("3 --> Iesire");
    }

    public Menu select() {
        String userInput = input.nextLine();
        if(userInput.equals("1")) {
            return new MathMenu(null);
        } else if(userInput.equals("2")) {
            return null; //probleme de informatica
        } else if(userInput.equals("3")) {
            return null;
        } else {
            System.out.println("Optiune invalida");
            return this;
        }
    }
}
