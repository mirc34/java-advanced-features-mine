package JavaAdvancedFeaturesCoding.WeekendTwo.Ex4;

public class MenuHandler {
    Menu currentMenu;
    public MenuHandler(Menu currentMenu) {
        this.currentMenu = currentMenu;
    }
    public void start() {
        while(currentMenu != null) {
            currentMenu.draw();
            currentMenu = currentMenu.select();
        }
    }
}
