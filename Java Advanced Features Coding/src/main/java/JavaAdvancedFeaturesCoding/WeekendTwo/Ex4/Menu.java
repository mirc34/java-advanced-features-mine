package JavaAdvancedFeaturesCoding.WeekendTwo.Ex4;

public interface Menu {
    void draw();
    Menu select();
}
