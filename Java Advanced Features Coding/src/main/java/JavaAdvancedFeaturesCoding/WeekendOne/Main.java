package JavaAdvancedFeaturesCoding.WeekendOne;

public class Main {
    public static void main(String[] args) {
        Program program = new CalculatorForme();
        ProgramHandler programHandler = new ProgramHandler(program);
        programHandler.start();
    }
}
