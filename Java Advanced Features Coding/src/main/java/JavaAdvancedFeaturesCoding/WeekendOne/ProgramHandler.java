package JavaAdvancedFeaturesCoding.WeekendOne;

import java.util.Scanner;

public class ProgramHandler {
    private Program program;
    private Scanner scanner = new Scanner(System.in);

    public ProgramHandler(Program program) {
        this.program = program;
    }

    private String getUserInput() {
        System.out.println("Hello, you started program handler, if you want to exit type quit.");
        System.out.println(program.getDescription());
        return scanner.nextLine();
    }

    public void start() {
        String input = getUserInput();
        while(!input.equals("quit")) {
            program.run(input);
            input = getUserInput();
        }
    }
}
