package JavaAdvancedFeaturesCoding.WeekendOne;

public interface Program {
     void run(String userInput);
     String getDescription();
}
