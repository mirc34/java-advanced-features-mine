package JavaAdvancedFeaturesCoding.WeekendOne;

public class GreetingsProgram implements Program{

    public void run(String userInput) {
        System.out.println("Hello " + userInput + "!");
        System.out.println("Your name has " + userInput.length() + " letters.");
    }

    public String getDescription() {
        return "What's your name?";
    }
}
