package JavaAdvancedFeaturesCoding.WeekendOne.Ex4;

import JavaAdvancedFeaturesCoding.WeekendOne.Program;

public class Exercitiul4 implements Program {

    public void run(String userInput) {
        String[] cuvinte = userInput.split(" |\\.|,|!|\\?");
        String cuvantMaxim = cuvinte[0];
        String cuvantMinim = cuvinte[0];
        for(String cuvant: cuvinte) {
            if(cuvant.isEmpty())
                continue;
            if(cuvantMaxim.length() < cuvant.length()) {
                cuvantMaxim = cuvant;
            } else if(cuvantMinim.length() > cuvant.length()) {
                cuvantMinim = cuvant;
            }
        }
        System.out.println("Cel mai lung cuvant este: " + cuvantMaxim);
        System.out.println("Cel mai scurt cuvant este: " + cuvantMinim);
    }

    public String getDescription() {
        return "Introdu o fraza";
    }
}
