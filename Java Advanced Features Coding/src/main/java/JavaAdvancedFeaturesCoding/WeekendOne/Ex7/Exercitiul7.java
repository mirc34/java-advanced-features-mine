package JavaAdvancedFeaturesCoding.WeekendOne.Ex7;

import JavaAdvancedFeaturesCoding.WeekendOne.Program;

public class Exercitiul7 implements Program {
    public static final String NORMAL_DESCRIPTION = "Introdu un numar natural";
    public static final String BAD_NUMBER_DESTIPION = " is not a number";
    public static final String NUMBER_OUT_OF_RANGE_DESCRIPTION = " is less than 1";

    private String description = Exercitiul7.NORMAL_DESCRIPTION;

    public void run(String userInput) {
        System.out.println(userInput);
        int N;
        try {
            N = Integer.parseInt(userInput);
            description = Exercitiul7.NORMAL_DESCRIPTION;
        } catch (Exception e) {
            description = userInput + Exercitiul7.BAD_NUMBER_DESTIPION;
            return;
        }
        if(N > 0) {
            System.out.println(fib(N));
        } else {
            description = userInput + Exercitiul7.NUMBER_OUT_OF_RANGE_DESCRIPTION;
        }
    }

    private int fib(int n) {
        if(n == 1)
            return 0;
        int first = 0;
        int second = 1;
        int fibNumber = second;
        for(int i = 2; i < n; i++){
            fibNumber = first + second;
            first = second;
            second = fibNumber;
        }
        return fibNumber;
    }

    public String getDescription() {
        return description;
    }
}
