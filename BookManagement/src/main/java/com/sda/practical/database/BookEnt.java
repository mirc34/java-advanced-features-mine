package com.sda.practical.database;

import javax.persistence.*;

@Entity
@Table(name="books")
public class BookEnt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer bookId;
    private String name;
    private String isbn;
    private String publishingHouse;
    private Integer year;

    @ManyToOne
    @JoinColumn(name="authorId")
    private AuthorEnt author;

//    @OneToMany(mappedBy = "book")
//    List<ReviewEnt> comments;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public AuthorEnt getAuthor() {
        return author;
    }

    public void setAuthor(AuthorEnt author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "BookEnt{" +
                "bookId=" + bookId +
                ", name='" + name + '\'' +
                ", isbn='" + isbn + '\'' +
                ", publishingHouse='" + publishingHouse + '\'' +
                ", year=" + year +
                ", author=" + author +
                '}';
    }
}
