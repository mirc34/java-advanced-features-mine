package com.sda.practical.database;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="author")
public class AuthorEnt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer authorID;
    private String name;

    @OneToMany(mappedBy = "author")
    private List<BookEnt> books;

    public Integer getAuthorID() {
        return authorID;
    }

    public void setAuthorID(Integer authorID) {
        this.authorID = authorID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BookEnt> getBooks() {
        return books;
    }

    public void setBooks(List<BookEnt> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "AuthorEnt{" +
                "authorID=" + authorID +
                ", name='" + name + '\'' + '}';
    }
}
