package com.sda.practical.services;

import com.sda.practical.database.AuthorEnt;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class AuthorServices extends BaseServices{

    public AuthorServices(HibernateService hibernateService) {
        super(hibernateService);
        System.out.println(getClass().getSimpleName() + " created");
    }
    public void viewAuthors() {
        List<AuthorEnt> authors = getAllAuthors();
        authors.stream().forEach(System.out::println);
    }

    public void editAuthor(Scanner scanner) {
        List<AuthorEnt> authors = getAllAuthors();
        authors.stream().forEach(a -> System.out.println(a.getAuthorID() + ". " + a.getName()));

        System.out.println("Selectati un autor: ");
        Integer authorId = scanner.nextInt();
        scanner.nextLine();

        AuthorEnt authorEnt = getAuthor(authorId);
        System.out.println("Introduceti numele autorului: ");
        String name = scanner.nextLine();

        authorEnt.setName(name);
        updateAuthor(authorEnt);
    }

    public void addAuthor(Scanner scanner){
        AuthorEnt authorEntity = new AuthorEnt();

        scanner.nextLine();
        System.out.println("Introduceti numele autorului: ");
        String name = scanner.nextLine();

        authorEntity.setName(name);
        saveAuthor(authorEntity);
    }

    public void deleteAuthor(Scanner scanner) {
        List<AuthorEnt> authors = getAllAuthors();
        authors.stream().forEach(a -> System.out.println(a.getAuthorID() + ". " + a.getName()));

        System.out.println("Introduceti ID-ul autorului dorit: ");
        Integer authorId = scanner.nextInt();
        scanner.nextLine();

        AuthorEnt authorEnt = getAuthor(authorId);
        deleteAuthor(authorEnt);
    }

    private List<AuthorEnt> getAllAuthors(){
        Session session = getHibernateService().getSessionFactory().openSession();
        return session.createQuery("from AuthorEnt", AuthorEnt.class).list();
    }


    private void saveAuthor(AuthorEnt authorEnt){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(authorEnt);
        transaction.commit();
        session.close();
    }


    private void updateAuthor(AuthorEnt authorEnt){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(authorEnt);
        transaction.commit();
        session.close();
    }

    private void deleteAuthor(AuthorEnt authorEnt){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(authorEnt);
        transaction.commit();
        session.close();
    }

}
