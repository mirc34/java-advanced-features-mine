package com.sda.practical.services;

import com.sda.practical.database.AuthorEnt;
import org.hibernate.Session;

public abstract class BaseServices {
    private HibernateService hibernateService;

    public BaseServices(HibernateService hibernateService){
        this.hibernateService = hibernateService;
    }

    public HibernateService getHibernateService() {
        return hibernateService;
    }

    protected AuthorEnt getAuthor(Integer authorId) {
        Session session = getHibernateService().getSessionFactory().openSession();
        AuthorEnt authorEnt = session.find(AuthorEnt.class, authorId);
        session.close();
        return authorEnt;
    }
}
