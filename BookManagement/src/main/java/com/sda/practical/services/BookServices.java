package com.sda.practical.services;

import com.sda.practical.database.AuthorEnt;
import com.sda.practical.database.BookEnt;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class BookServices extends BaseServices {

    public BookServices(HibernateService hibernateService) {
        super(hibernateService);
        System.out.println(getClass().getSimpleName() + " created");
    }

    public void viewBooks() {
        List<BookEnt> books = getAllBooks();
        books.stream().forEach(System.out::println);
    }

    public void editBook(Scanner scanner) {
        List<BookEnt> books = getAllBooks();
        books.stream().forEach(a -> System.out.println(a.getBookId() + " " + a.getAuthor() + " " + a.getName() + " " + a.getIsbn() + " " + a.getPublishingHouse() + " " + a.getYear()));

        System.out.println("Introduceti bookId pentru care doriti sa se faca editarea: ");
        Integer bookId = scanner.nextInt();
        scanner.nextLine();

        BookEnt bookEntity = getBook(bookId);
        System.out.println("Introduceti numele cartii: ");
        String name = scanner.nextLine();

        System.out.println("Introduceti codul ISBN: ");
        String isbn = scanner.nextLine();

        System.out.println("Introduceti editura cartii: ");
        String editura = scanner.nextLine();

        System.out.println("Introduceti anul cartii: ");
        Integer an = scanner.nextInt();

        System.out.println("Introduceti ID-ul autorului: ");
        Integer id = scanner.nextInt();
        AuthorEnt author = getAuthor(id);

        bookEntity.setName(name);
        bookEntity.setIsbn(isbn);
        bookEntity.setPublishingHouse(editura);
        bookEntity.setYear(an);
        bookEntity.setAuthor(author);

        updateBook(bookEntity);
    }

    public void addBook(Scanner scanner){
        BookEnt bookEnt = new BookEnt();

        scanner.nextLine();
        System.out.println("Introduceti numele cartii: ");
        String name = scanner.nextLine();
        System.out.println("Introduceti ISBN-ul: ");
        String ISBN = scanner.nextLine();
        System.out.println("Introduceti editura: ");
        String editura = scanner.nextLine();
        System.out.println("Introduceti anul: ");
        Integer an = scanner.nextInt();

        bookEnt.setName(name);
        bookEnt.setIsbn(ISBN);
        bookEnt.setPublishingHouse(editura);
        bookEnt.setYear(an);
        saveBook(bookEnt);
    }

    public void deleteBook(Scanner scanner) {
        List<BookEnt> books = getAllBooks();
        books.stream().forEach(a -> System.out.println(a.getBookId() + ". " + a.getName()));

        System.out.println("Introduceti ID-ul cartii dorite: ");
        Integer bookId = scanner.nextInt();
        scanner.nextLine();

        BookEnt bookEnt = getBook(bookId);
        deleteBook(bookEnt);
    }

    private List<BookEnt> getAllBooks() {
        Session session = getHibernateService().getSessionFactory().openSession();
        return session.createQuery("from BookEnt", BookEnt.class).list();
    }

    private BookEnt getBook(Integer bookId) {
        Session session = getHibernateService().getSessionFactory().openSession();
        BookEnt bookEnt = session.find(BookEnt.class, bookId);
        session.close();
        return bookEnt;
    }

    private void updateBook(BookEnt bookEnt){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(bookEnt);
        transaction.commit();
        session.close();
    }

    private void saveBook(BookEnt bookEnt){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(bookEnt);
        transaction.commit();
        session.close();
    }

    private void deleteBook(BookEnt bookEnt){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(bookEnt);
        transaction.commit();
        session.close();
    }

}
