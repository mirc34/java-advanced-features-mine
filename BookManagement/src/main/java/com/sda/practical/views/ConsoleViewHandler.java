package com.sda.practical.views;

import com.sda.practical.services.AuthorServices;
import com.sda.practical.services.BookServices;
import com.sda.practical.services.HibernateService;

import java.util.Scanner;

public class ConsoleViewHandler {

    private MenuHandler menuHandler;
    private AuthorServices authorServices;
    private BookServices bookServices;
    private HibernateService hibernateService;

    public ConsoleViewHandler() {
        this.menuHandler = new MenuHandler();
        this.hibernateService = new HibernateService();
        this.authorServices = new AuthorServices(hibernateService);
        this.bookServices = new BookServices(hibernateService);
        System.out.println(getClass().getSimpleName() + " created");
    }
    public void start() {
        Scanner scanner = new Scanner(System.in);
        Integer option = 0;
        while(option != 9){
            menuHandler.printMenu(MenuTypeEnum.MAIN_MENU);
            option = scanner.nextInt();
            switch(option){
                case 1:
                    authorFlow(scanner);
                    break;
                case 2:
                    bookFlow(scanner);
                    break;
                case 3:
                    menuHandler.printMenu(MenuTypeEnum.REVIEW_MENU);
                    option = scanner.nextInt();
                    break;
                default:
                    System.out.println("Goodbye!");
            }
        }
    }

    public void authorFlow(Scanner scanner){
        Integer option = 0;
        while(option != 9){
            menuHandler.printMenu(MenuTypeEnum.AUTHOR_MENU);
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    this.authorServices.viewAuthors();
                    break;
                case 2:
                    this.authorServices.addAuthor(scanner);
                    break;
                case 3:
                    this.authorServices.editAuthor(scanner);
                    break;
                case 4:
                    this.authorServices.deleteAuthor(scanner);
                default:
                    System.out.println("apasa 9 pentru iesire");
            }
        }
    }

    public void bookFlow(Scanner scanner) {
        Integer option = 0;
        while(option != 9) {
            menuHandler.printMenu(MenuTypeEnum.BOOK_MENU);
            option = scanner.nextInt();
            switch(option) {
                case 1:
                    this.bookServices.viewBooks();
                    break;
                case 2:
                    this.bookServices.addBook(scanner);
                    break;
                case 3:
                    this.bookServices.editBook(scanner);
                    break;
                case 4:
                    this.bookServices.deleteBook(scanner);
                    break;
                default:
                    System.out.println("Apasa 9 pentru iesire.");
            }
        }
    }
}
