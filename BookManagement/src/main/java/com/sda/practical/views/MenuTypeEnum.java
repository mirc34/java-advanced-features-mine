package com.sda.practical.views;

public enum MenuTypeEnum {
    MAIN_MENU,
    AUTHOR_MENU,
    BOOK_MENU,
    REVIEW_MENU;
}
