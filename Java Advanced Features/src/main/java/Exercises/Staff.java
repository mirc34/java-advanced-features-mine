package Exercises;

public class Staff extends Person{

    private String specialization;
    private int salary;

    public Staff(String name, String address, String specialization, int salary) {
        super(name, address);
        this.specialization = specialization;
        this.salary = salary;
    }

    @Override
    public void priceOrSalary() {
        System.out.println(salary);
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Staff{" + "name -> '" + super.name + "\'" + ", address -> '" + super.address + "\'" +", specialization -> '" + specialization + '\'' + ", salary -> " + salary + '}';
    }

}
