package Exercises;

public class Student extends Person{

    private String study;
    private int yearOfStudy;
    private int studyPrice;

    public Student(String name, String address, String study, int yearOfStudy, int studyPrice) {
        super(name, address);
        this.study = study;
        this.yearOfStudy = yearOfStudy;
        this.studyPrice = studyPrice;
    }

    @Override
    public void priceOrSalary() {
        System.out.println(studyPrice);
    }

    public String getStudy() {
        return study;
    }

    public void setStudy(String study) {
        this.study = study;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public int getStudyPrice() {
        return studyPrice;
    }

    public void setStudyPrice(int studyPrice) {
        this.studyPrice = studyPrice;
    }

    @Override
    public String toString() {
        return "Student{" + "name -> '" + super.name + '\''+ ", address -> '" + super.address + '\'' + ", study -> '" + study + '\'' + ", yearOfStudy -> " + yearOfStudy + ", studyPrice -> " + studyPrice + '}';
    }



}
