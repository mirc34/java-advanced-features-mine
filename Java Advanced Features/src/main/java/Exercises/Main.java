package Exercises;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args){

        Student student1 = new Student("Alex","Suceava","Programare", 2, 1500);
        Student student2 = new Student("Andrei","Prahova","Arhitectura", 1, 1000);
        Student student3 = new Student("Marian","Bucuresti","Filologie", 3, 750);

        Staff staff1 = new Staff("Rares", "Iasi", "Profesor", 1200);
        Staff staff2 = new Staff("Codrut", "Constanta", "Programator", 1500);
        Staff staff3 = new Staff("Gabriel", "Timisoara", "Constructor", 1000);

        List<Person> students = new ArrayList<Person>();
        List<Person> staff = new ArrayList<Person>();

        students.add(student1);
        students.add(student2);
        students.add(student3);

        staff.add(staff1);
        staff.add(staff2);
        staff.add(staff3);

        nameAndAddress(student1);

        printStudents(students);

        printStaff(staff);



    }

    public static void nameAndAddress(Person obj) {

        System.out.println("Method nameAndAddress");
        System.out.println(obj.getAddress());
        System.out.println(obj.getName());

    }

    public static void printStudents(List<Person> students) {

        for(int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i));
        }

    }

    public static void printStaff(List<Person> staff) {

        for(Person obj: staff) {
            System.out.println(obj);
        }

    }

}
