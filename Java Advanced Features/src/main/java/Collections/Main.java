package Collections;

import Exercises.Person;
import Exercises.Staff;
import Exercises.Student;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<String> visitedCountries = new ArrayList<>();
        visitedCountries.add("Germany");
        visitedCountries.add("France");
        visitedCountries.add("Spain");
        visitedCountries.add("France");

        for(String country : visitedCountries)
            System.out.print(country + " ");

        Iterator<String> iterator = visitedCountries.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

        Set<String> colours = new HashSet<>();
        colours.add("Verde");
        colours.add("Rosu");
        colours.add("Albastru");
        colours.add("Violet");
        colours.add("Alb");

        for(String color: colours)
            System.out.print(color + " ");

        System.out.printf("\n ---------------- \n");

        Set<String> colours2 = new TreeSet<>();
        colours2.add("Verde");
        colours2.add("Rosu");
        colours2.add("Albastru");
        colours2.add("Violet");
        colours2.add("Alb");

        for(String color: colours2)
            System.out.print(color + " ");

        System.out.println("\n -------------");

        Set<Integer> integers = new TreeSet<>();
        integers.add(1);
        integers.add(5);
        integers.add(3);
        integers.add(20);
        integers.add(15);

        for(Integer integer: integers){
            System.out.println(integer);
        }

        Map<String,Integer> students = new HashMap<>();

        students.put("George", 9);
        students.put("Paul", 8);
        students.put("Ioana", 10);
        students.put("Laura", 5);
        students.put("Andreea", 7);

        for(Map.Entry<String, Integer> student: students.entrySet()){
//            System.out.println(student);
            String key = student.getKey();
            Integer value = student.getValue();
            System.out.printf("%s are nota %d \n", key, value);
        }

        for(Integer nota: students.values()){
            System.out.println(nota);
        }

        System.out.println("Nota Ioanei este: " + students.get("Ioana"));

        Map<String, Person> persons = new HashMap<>();
        Person student = new Student("Alex","Suceava","Programare", 2, 1500);
        Person staff = new Staff("Rares", "Iasi", "Profesor", 1200);

        persons.put("Student", student);
        persons.put("Staff", staff);

        for(Map.Entry<String, Person> person: persons.entrySet()) {

            System.out.println("Persoana se numeste " + person.getValue().getName() + " si locuieste in " + person.getValue().getAddress() + ".");

        }

    }

}
