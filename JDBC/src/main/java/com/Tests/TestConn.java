package com.Tests;

import java.sql.*;

public class TestConn {
    public static void main(String[] args) {
        Connection con = null;
        try {
//            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Bombonel1!&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection Succesul");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM customers");
            while(rs.next()) {
                Integer deptId = rs.getInt("customerNumber");
                String deptName = rs.getString("customerName");
                System.out.println(deptId + " " + deptName);
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
