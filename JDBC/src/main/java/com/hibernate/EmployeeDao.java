package com.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class EmployeeDao {
    public void findByID(int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            EmployeeEnt employee = session.find(EmployeeEnt.class, id);
            System.out.println(employee);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void save(EmployeeEnt employee){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(employee);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void update(EmployeeEnt employee, int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            employee.setEmployeeNumber(id);
            session.update(employee);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            EmployeeEnt employee = null;
            employee = session.load(EmployeeEnt.class, id);
            session.delete(employee);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
