package com.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class AlienDao {
    public void createAlien(AlienEnt alien){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(alien);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void updateAlien(AlienEnt alien, int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            alien.setAlienId(id);
            session.update(alien);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void deleteAlien(AlienEnt alien, int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            alien.setAlienId(id);
            session.delete(alien);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
