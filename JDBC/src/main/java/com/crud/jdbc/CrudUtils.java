package com.crud.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CrudUtils {

    public void selectProducts(int a, int b){
        Connection con = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Bombonel1!&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection Succesul");
            ResultSet rs = null;
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM products WHERE buyPrice BETWEEN ? AND ?");

            stmt.setDouble(1, a);
            stmt.setDouble(2, b);

            rs = stmt.executeQuery();
            while(rs.next()) {
                    double buyPrice = rs.getDouble("buyPrice");
                    System.out.println("Pret vanzare in intervalul dorit de noi:" + buyPrice);
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateProduct(String a, String b) {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Bombonel1!&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection Succesul");
            PreparedStatement stmt = con.prepareStatement("UPDATE products SET productName = ? WHERE productCode = ?");

            stmt.setString(1, a);
            stmt.setString(2, b);

            int size = stmt.executeUpdate();

            if(size > 0)
                System.out.println("Update done");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void insertProduct(String productCode, String productName, String productLine, String productScale, String productVendor, String productDescription, int quantityInStock, double buyPrice, double MSRP) {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=Bombonel1!&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection Succesul");
            PreparedStatement stmt = con.prepareStatement("INSERT INTO products(productcode, productname, productline, productscale, productvendor, productdescription, quantityinstock, buyprice, msrp) \n " +
                                                              "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");

            stmt.setString(1, productCode);
            stmt.setString(2, productName);
            stmt.setString(3, productLine);
            stmt.setString(4, productScale);
            stmt.setString(5, productVendor);
            stmt.setString(6, productDescription);
            stmt.setInt(7, quantityInStock);
            stmt.setDouble(8, buyPrice);
            stmt.setDouble(9, MSRP);

            boolean size = stmt.execute();

            if(size)
                System.out.println("Insert done");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
