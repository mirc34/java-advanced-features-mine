package DesignPatterns.builder;

public class Home {

    private String foundation;
    private String basement;
    private String walls;
    private String roof;
    private int roomNumber;

    public String getFoundation() {
        return foundation;
    }

    public void setFoundation(String foundation) {
        this.foundation = foundation;
    }

    public String getBasement() {
        return basement;
    }

    public void setBasement(String basement) {
        this.basement = basement;
    }

    public String getWalls() {
        return walls;
    }

    public void setWalls(String walls) {
        this.walls = walls;
    }

    public String getRoof() {
        return roof;
    }

    public void setRoof(String roof) {
        this.roof = roof;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    @Override
    public String toString() {
        return "Home{" +
                "foundation='" + foundation + '\'' +
                ", basement='" + basement + '\'' +
                ", walls='" + walls + '\'' +
                ", roof='" + roof + '\'' +
                ", roomNumber=" + roomNumber +
                '}';
    }

    public static class HomeBuilder {
        private Home home;

        public HomeBuilder(String foundation, String walls, String roof) {
            this.home = new Home();
            home.foundation = foundation;
            home.walls = walls;
            home.roof = roof;
        }

        public HomeBuilder addBasement(String basement) {
            home.basement = basement;
            return this;
        }

        public HomeBuilder addRoomNumber(int roomNumber) {
            home.roomNumber = roomNumber;
            return this;
        }

        public Home build() {
            return home;
        }
    }
}
