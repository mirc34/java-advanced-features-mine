package DesignPatterns.builder;

public class Main {
    public static void main(String[] args) {
        Home.HomeBuilder homeBuilder = new Home.HomeBuilder("Beton", "lemn", "Sarpanta");
        Home basicHome = homeBuilder.build();
        Home.HomeBuilder castleBuilder = new Home.HomeBuilder("Beton","Caramida","Sarpanta");
        Home complexHome = castleBuilder.addBasement("Demisol").addRoomNumber(10).build();
        System.out.println(basicHome);
        System.out.println(complexHome);
    }
}
