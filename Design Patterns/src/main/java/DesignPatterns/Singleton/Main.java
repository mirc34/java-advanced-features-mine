package DesignPatterns.Singleton;

public class Main {
    public static void main(String[] args) {
        Logging loggingService = Logging.getInstance();
        loggingService.printMessage("Am creat serviciul");

        Logging loggingService2 = Logging.getInstance();
        loggingService2.printMessage("Al doilea mesaj");
        System.out.println(loggingService);
    }
}
