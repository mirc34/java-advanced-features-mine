package DesignPatterns.AbstractFactory;

import DesignPatterns.AbstractFactory.factories.AbstractFactory;
import DesignPatterns.AbstractFactory.factories.PizzaFactory;
import DesignPatterns.AbstractFactory.pizza.Pizza;
import DesignPatterns.AbstractFactory.pizza.PizzaType;

public class Main {

    public static void main(String[] args) {
        AbstractFactory pizzaFactory = new PizzaFactory();
        Pizza comanda1 = pizzaFactory.create(PizzaType.QUATTROFORMAGGI, 30);
        Pizza comanda2 = pizzaFactory.create(PizzaType.MARGHERITA, 50);
        Pizza comanda3 = pizzaFactory.create(PizzaType.QUATTROSTAGIONi, 35);
        System.out.println(comanda1);
    }

}
