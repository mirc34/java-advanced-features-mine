package DesignPatterns.AbstractFactory.pizza;

public class PizzaQuattroStagioni extends Pizza{

    private int size;

    public PizzaQuattroStagioni(int size){
        this.size = size;
    }

    public String getName() {
        return PizzaType.QUATTROSTAGIONi.toString();
    }

    public String getIngredients() {
        return "Mozzarella, carne, ciuperci, sos rosii";
    }

    public int getSize() {
        return size;
    }
}
