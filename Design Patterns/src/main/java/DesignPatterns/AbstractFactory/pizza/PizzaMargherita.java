package DesignPatterns.AbstractFactory.pizza;

public class PizzaMargherita extends Pizza {

    private int size;

    public PizzaMargherita(int size){
        this.size = size;
    }

    public String getName() {
        return PizzaType.MARGHERITA.toString();
    }

    public String getIngredients() {
        return "Mozzarella, sos rosii";
    }

    public int getSize() {
        return size;
    }
}
