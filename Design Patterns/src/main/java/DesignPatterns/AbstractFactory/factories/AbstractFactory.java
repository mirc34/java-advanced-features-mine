package DesignPatterns.AbstractFactory.factories;

import DesignPatterns.AbstractFactory.pizza.Pizza;
import DesignPatterns.AbstractFactory.pizza.PizzaType;

public interface AbstractFactory {
    Pizza create(PizzaType type, int size);
}
