package DesignPatterns.AbstractFactory.factories;

import DesignPatterns.AbstractFactory.pizza.*;

public class PizzaFactory implements AbstractFactory {
    public Pizza create(PizzaType type, int size) {
        switch(type) {
            case MARGHERITA:
                Pizza margherita = new PizzaMargherita(size);
                return margherita;
            case QUATTROFORMAGGI:
                Pizza quattroformaggi = new PizzaQuattroFormaggi(size);
                return quattroformaggi;
            case QUATTROSTAGIONi:
                Pizza quattrostagioni = new PizzaQuattroStagioni(size);
                return quattrostagioni;
            default:
                System.out.println("Alta pizza");
                return null;
        }
    }
}
