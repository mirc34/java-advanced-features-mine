package SOLID;

public interface Car {

    public void start();
    public void accelerate(int speed);
    public void stop();

}
