package SOLID;

public class MotorCar implements Car{

    private Engine engine;

    public MotorCar() {
        this.engine = new Engine();
    }

    public void start() {
        System.out.println("I'm a motor car");
        engine.run();
    }

    public void accelerate(int speed) {
        engine.speed(speed);
    }

    public void stop() {
        engine.stop();
    }

}
