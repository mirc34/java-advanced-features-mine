package SOLID;
public class Engine {

    public void run() {
        System.out.println("Engine is running");
    }

    public void stop() {
        System.out.println("Engine has stopped");
    }

    public void speed(int speed) {
        System.out.println("The speed is " + speed + " km/h");
    }

}
