package SOLID;

public class ElectricCar implements Car{

    private Engine engine;

    public ElectricCar() {
        this.engine = new Engine();
    }

    public void start() {
        System.out.println("I'm an electric car");
        engine.run();
    }

    public void accelerate(int speed) {
        engine.speed(speed);
    }

    public void stop() {
        engine.stop();
    }

    public void regenerativeBrake() {
        System.out.println("Regenerating battery");
        engine.stop();
    }

}
