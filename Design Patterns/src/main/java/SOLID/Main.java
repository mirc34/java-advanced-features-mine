package SOLID;

public class Main {

    public static void main(String[] args) {

        Car car = new MotorCar();
        ElectricCar eCar = new ElectricCar();
        drive(car);
        drive(eCar);
        specificDrive(eCar);

    }

    public static void drive(Car car) {
        car.start();
        car.accelerate(160);
        car.stop();
    }

    public static void specificDrive(ElectricCar electricCar) {
        electricCar.start();
        electricCar.accelerate(200);
        electricCar.stop();
        electricCar.regenerativeBrake();
    }

}
