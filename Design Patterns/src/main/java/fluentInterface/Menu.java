package fluentInterface;

import java.util.List;

public interface Menu {

    public Menu orderPizza(List<String> pizzas);
    public Menu eatPizza();
    public Menu payPizza();

}
