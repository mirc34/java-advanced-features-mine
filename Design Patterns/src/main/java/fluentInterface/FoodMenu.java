package fluentInterface;

import java.util.ArrayList;
import java.util.List;

public class FoodMenu implements Menu {

    List<Pizza> pizzaList;
    List<String> selectedPizzas;

    public FoodMenu() {
        this.pizzaList = new ArrayList<Pizza>();
        populateMenu();
        selectedPizzas = new ArrayList<>();
    }

    private void populateMenu() {
        Pizza object = new PizzaMargherita();
        pizzaList.add(object);
    }

    public Menu orderPizza(List<String> pizzas) {
        for(String order: pizzas) {
            System.out.println("I have ordered pizza" + " " + order);
            selectedPizzas.add(order);
        }
        return this;
    }

    public Menu eatPizza() {
        System.out.println("I have eaten the pizza.");
        return this;
    }

    public Menu payPizza() {
        System.out.println("I payed for the pizza");
        return this;
    }

}
